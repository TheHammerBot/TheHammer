"""
    The Hammer
    Copyright (C) 2018 JustMaffie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

from discord.ext import commands
import discord
from thehammer.module import Module
import aiohttp
import datetime

now = lambda: datetime.datetime.utcnow()


class Status(Module):
    async def get_session(self):
        if not hasattr(self, "session"):
            self.session = aiohttp.ClientSession()
        return self.session

    async def get(self, api_url, headers={}):
        session = await self.get_session()
        resp = await session.get(api_url, headers=headers)
        content = await resp.json()
        resp.close()
        return content

    def get_value(self, list, key):
        for entry in list:
            if entry['name'] == key:
                return entry
        return {}

    @commands.group()
    async def status(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send_help()

    @status.command()
    async def discord(self, ctx):
        INDICATOR_COLORS = {
            "none": discord.Colour.green(),
            "partial": discord.Colour.orange(),
            "major": discord.Colour.red()
        }
        url = "https://srhpyqt94yxb.statuspage.io/api/v2/summary.json"
        data = await self.get(url)
        indicator = data['status']['indicator']
        color = INDICATOR_COLORS.get(indicator, discord.Colour.blue())
        embed = discord.Embed(color=color, timestamp=now())
        embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)
        api = self.get_value(data['components'], "API")['status']
        embed.add_field(name="API", value=api.title(), inline=False)
        gateway = self.get_value(data['components'], "Gateway")['status']
        embed.add_field(name="Gateway", value=gateway.title(), inline=False)
        proxy = self.get_value(data['components'], "Media Proxy")['status']
        embed.add_field(name="Media Proxy", value=proxy.title(), inline=False)
        cf = self.get_value(data['components'], "CloudFlare")['status']
        embed.add_field(name="CloudFlare", value=cf.title(), inline=False)
        voice = self.get_value(data['components'], "Voice")['status']
        embed.add_field(name="Voice", value=voice.title(), inline=False)
        embed.set_footer(text=data['status']['description'])
        return await ctx.send(embed=embed)

    @status.command()
    async def fortnite(self, ctx):
        INDICATOR_COLORS = {
            "none": discord.Colour.green(),
            "partial": discord.Colour.orange(),
            "major": discord.Colour.red()
        }
        data = await self.bot.fortnite.get_status()
        if not 'fortnite' in data:
            return await ctx.send("Error 123")
        data = data['fortnite']
        indicator = "none"
        for key, value in data.items():
            if value['status'] == 'partial':
                if indicator != 'major':
                    indicator = 'partial'
            if value['status'] == 'major':
                indicator = 'major'
        color = INDICATOR_COLORS.get(indicator, discord.Colour.blue())
        embed = discord.Embed(color=color, timestamp=now())
        embed.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)
        for key, value in data.items():
            name = value['name']
            status = value['status'].title()
            embed.add_field(name=name, value=status, inline=False)
        # embed.set_footer(text=data['status']['description'])
        return await ctx.send(embed=embed)


def setup(bot):
    bot.load_module(Status)

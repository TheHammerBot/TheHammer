"""
    The Hammer
    Copyright (C) 2018 JustMaffie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import discord
import datetime


class Case:
    def __init__(self, bot, guild, data):
        fields = ['case_id', 'type', 'moderator',
                  'user', 'reason', 'message', 'timestamp']
        for field in fields:
            if not field in data:
                raise SyntaxError("Invalid Case")
        self.bot = bot
        self.guild = guild
        self.data = data

    def get(self, key, default=None):
        return self.data.get(key, default)

    async def set(self, key, value):
        self.data[key] = value

    async def save(self):
        filter = {
            "case_id": self.get("case_id"),
            "guild_id": self.get("guild_id")
        }
        update = {
            "$set": self.data
        }
        await self.bot.db.cases.update_one(filter, update)
        em = await self.generate_embed()
        try:
            c = await self.guild.settings.get("modlog_channel", 1)
            msg = self.get("message")
            message = await self.bot.get_channel(c).get_message(msg)
            await message.edit(embed=em)
        except:
            pass

    async def generate_embed(self):
        title = "{} | Case: #{}"
        type = self.get("type", "Invalid Type")
        id = self.get("case_id", 1)
        color = self.get_case_color(type)
        time = self.get("timestamp", None)
        em = discord.Embed(title=title.format(type, id),
                           color=color, timestamp=time)
        user = await self.bot.get_user_info(self.get("user"))
        try:
            moderator = await self.bot.get_user_info(self.get("moderator"))
        except Exception:
            moderator = None
        if not moderator:
            moderator = "Unknown Moderator"
        msg = "{} (<@{}>)"
        _user = str(user)
        _id = user.id
        em.add_field(name="User", value=msg.format(_user, _id), inline=True)
        em.add_field(name="Moderator", value=str(moderator), inline=True)
        em.add_field(name="Reason", value=self.get("reason"), inline=False)
        em.set_footer(text='ID: {}'.format(user.id))
        return em

    def get_case_color(self, _type):
        if _type.lower() == 'kick' or _type.lower() == 'mute':
            return discord.Colour.gold()
        if _type.lower() == 'ban':
            return discord.Colour.red()
        else:
            return discord.Colour.green()


class ModLog:
    def __init__(self, bot):
        self.bot = bot

    async def get_guild(self, guild):
        return ModGuild(self.bot, guild)


class ModGuild:
    def __init__(self, bot, guild):
        self.bot = bot
        self.guild = guild
        self.id = guild.id
        self.settings = GuildSettings(bot, self)

    async def generate_id(self):
        casecount = await self.settings.get("latest_case", 0)
        casecount = casecount + 1
        await self.settings.set("latest_case", casecount)
        await self.settings.save()
        return casecount

    async def get_case(self, _id):
        filter = {
            "case_id": _id,
            "guild_id": self.guild.id
        }
        case = await self.bot.db.cases.find_one(filter)
        if not case:
            return None
        case['id'] = _id
        del case['_id']
        return Case(self.bot, self, case)

    async def generate_embed(self, _id, _type,
                             moderator, user, reason, timestamp):
        title = "{} | Case: #{}"
        color = self.get_case_color(_type)
        em = discord.Embed(title=title.format(_type, _id),
                           color=color, timestamp=timestamp)
        user = await self.bot.get_user_info(user)
        try:
            moderator = await self.bot.get_user_info(moderator)
        except Exception as e:
            moderator = None
        if not moderator:
            moderator = "Unknown Moderator"
        _user = "{} (<@{}>)"
        _user = _user.foramt(str(user), user.id)
        em.add_field(name="User", value=_user, inline=True)
        em.add_field(name="Moderator", value=str(moderator), inline=True)
        em.add_field(name="Reason", value=reason, inline=False)
        em.set_footer(text='ID: {}'.format(user.id))
        return em

    async def update_setting(self, key, value):
        await self.settings.set(key, value)
        await self.settings.save()

    def get_case_color(self, _type):
        if _type.lower() == 'kick' or _type.lower() == 'mute':
            return discord.Colour.gold()
        if _type.lower() == 'ban':
            return discord.Colour.red()
        else:
            return discord.Colour.green()

    async def new_case(self, _type, user, moderator, reason=None):
        modlog_channel = await self.settings.get("modlog_channel")
        if not modlog_channel:
            return
        modlog_channel = self.bot.get_channel(modlog_channel)
        if not modlog_channel:
            return
        _id = await self.generate_id()
        if not reason:
            reason = "No reason set, set one with [p]reason {} <reason>"
            reason = reason.format(_id)
        timestamp = datetime.datetime.utcnow()
        if not moderator:
            moderator = "Unknown Moderator"
        if hasattr(moderator, "id"):
            moderator = moderator.id
        embed = await self.generate_embed(_id, _type,
                                          moderator, user.id, reason,
                                          timestamp)
        message = await modlog_channel.send(embed=embed)
        data = {
            "case_id": _id,
            "guild_id": self.id,
            "type": _type,
            "moderator": moderator,
            "user": user.id,
            "reason": reason,
            "message": message.id,
            "timestamp": timestamp
        }
        await self.bot.db.cases.insert_one(data)
        msg = "Created case id {} guild_id {}"
        self.bot.logger.info(msg.format(_id, self.id))
        return Case(self.bot, self, data)


class GuildSettings:
    def __init__(self, bot, guild):
        self.bot = bot
        self.guild = guild
        self.settings = None

    async def _get_settings(self):
        filter = {
            "_id": self.guild.id
        }
        coll = self.bot.db.guild_settings
        settings = await coll.find_one(filter)
        if not settings:
            await coll.insert_one(filter)
            settings = {}
        self.settings = settings

    async def set(self, setting, value):
        if not self.settings:
            await self._get_settings()
        self.settings[setting] = value

    async def get(self, setting, default=None):
        if not self.settings:
            await self._get_settings()
        return self.settings.get(setting, default)

    async def save(self):
        filter = {
            "_id": self.guild.id
        }
        if not self.settings:
            return
        _set = {
            "$set": self.settings
        }
        await self.bot.db.guild_settings.update_one(filter, _set)
